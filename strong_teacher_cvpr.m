function strongIndex = strong_teacher_cvpr(h)
    h1 = sort(h, 2, 'descend');
    h1 = h1(:, 1:2);
    cc = h1(:,1) - h1(:,2);
    strongIndex = find(cc<0.08);
end