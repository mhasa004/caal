clear all; close all;

addpath(genpath('tools/UGM'))
addpath('tools/softmax/');
addpath('tools/minFunc/');

load('data/allEvents_virat_c3d.mat');
%% Parameters
pc.dataset = 'virat';
pc.numSeq = length(allEvents);
pc.numClasses = 12; 
pc.lambda = 1e-1;
pc.numObjectClasses = 6;
pc.delta = 0.9;
pc.K = 0.4;
pc.withOP = 1;

pc.pBins = 5;
pc.ovBins = 6;
pc.aa_thr_t = 400;
pc.aa_thr_s = 800;
pc.ao_thr = 400;
pc.coFreqAA_a = 0;
pc.coFreqAA_m = 4;
pc.contextAP_a = 40;
pc.contextAP_m = 2;
pc.coFreqAO_a = 40;
pc.coFreqAO_m = 2;
pc.ONodeTweak = 0.01;
pc.PNodeTweak = 0.01;

pc.numBatch = 5;
pc.trainSeq = 1:176;
pc.testSeq = setdiff(1:pc.numSeq, pc.trainSeq);

%% experimenting with different teacher selections
pc.strongTeacher = 1;
pc.weakTeacher = 1;
run incremental_learning_context.m
