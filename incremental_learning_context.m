fprintf('Strong Teacher - %d, Weak Teacher - %d\n', pc.strongTeacher, pc.weakTeacher);
%% Arrange test data
testFeatures = [];
testLabels = [];

for i = pc.testSeq
    for j = 1:allEvents{i}.numEvents
        if abs(sum(allEvents{i}.features(:,j))) > 0
            testFeatures = [testFeatures, allEvents{i}.features(:,j)];
            testLabels = [testLabels, allEvents{i}.eventTypes(j)];
        end
    end
end

pc.inputSize = size(testFeatures, 1);

if pc.withOP
    testPDist = [];
    testPOverlap = [];
    for i = pc.testSeq
        for j = 1:allEvents{i}.numEvents
            if abs(sum(allEvents{i}.features(:,j))) > 0
                % get distance travled by a person
                for k = 1:length(allEvents{i}.mapObjectsTypes{j})
                    if allEvents{i}.mapObjectsTypes{j}(k)==1
                        flag = 1;
                        fl = allEvents{i}.mapObjectsFirstLocation{j}(k,1:2);
                        ll = allEvents{i}.mapObjectsLastLocation{j}(k,1:2);
                        dist = norm(fl-ll,2);
                        bin = bin_P_dist(dist);
                        testPDist = [testPDist, bin];
                    end
                end 
            end
        end
    end
    % Get associated objects for each event
    testObjectsList = compute_object_list(allEvents, pc.numObjectClasses, pc.testSeq);
end

% Make Adjacency Matrix and EdgeStruct on test data
testNumANode = size(testFeatures, 2);
testnStates = pc.numClasses * ones(1, testNumANode); 

if pc.withOP
    testNumONode = size(testObjectsList, 2);
    testNumPNode = size(testObjectsList, 2);
    testnStates = [testnStates, (pc.numObjectClasses-1) * ones(1, testNumONode)];
    testnStates = [testnStates, pc.pBins * ones(1, testNumPNode)];
end

testMaxState = max(testnStates); % Maximum number of states that any node can take
testnNodes = length(testnStates); % Total number of nodes

%% Compute the adjancecy matrix
testAdj = zeros(testnNodes);

% Link between activity and activity
[testAAdj, testTemporalDist, testSpatialDist] = ...
    make_adjacency(allEvents, pc.testSeq, pc.aa_thr_t, pc.aa_thr_t);
assert(size(testAAdj,1)==testNumANode);

testAdj(1:testNumANode, 1:testNumANode) = testAAdj;

testANodes = 1:testNumANode;
nodeNo = testNumANode;

%% Link between activity,object,and people
if pc.withOP
    % activity-object
    for i = 1:testNumANode
        nodeNo = nodeNo + 1;
        testAdj(i, nodeNo) = 1;
    end
    testONodes = testNumANode+1:testNumANode+testNumONode;

    % activity-people
    PNodes = [];
    for i = 1:size(testObjectsList,2)
        if testObjectsList(1,i)==1
            nodeNo = nodeNo + 1;
            PNodes = [PNodes, nodeNo];
            testAdj(i, nodeNo) = 1;
        end
    end
end

testAdj = testAdj + testAdj';
testEdgeStruct = UGM_makeEdgeStruct(testAdj,testnStates);

%% Compute number of training instances
numTrainingInstances = 0;
for i = pc.trainSeq
    for j = 1:allEvents{i}.numEvents
        if abs(sum(allEvents{i}.features(:,j))) > 0
           numTrainingInstances = numTrainingInstances + 1;
        end
    end 
end
trainInstancesPerIter = floor(numTrainingInstances / pc.numBatch);

%% Learn incrementally
% Initialization of edge cotnext matrices.
coFreqAA = zeros(pc.numClasses);
if pc.withOP
    coFreqAO = zeros(pc.numClasses, pc.numObjectClasses);
    contextAP = zeros(pc.numClasses, pc.pBins);
end

bufferTrainFeatures = [];
bufferTrainLabels = [];
oldSize = 0;
acc1 = []; acc2 = []; acc3 = [];

resContext_decode = zeros(pc.numBatch, testNumANode);
resContext_infer = zeros(pc.numBatch, testNumANode);
res_noContext = zeros(pc.numBatch, testNumANode);

totalTrainInstances = 0;
totalManLabledIns = 0;
iteration = 1;
curTrainSeq = [];

for i = pc.trainSeq    
    % Arrange data for the new video sequence 
    newTrainFeatures = [];
    newTrainLabels = [];
    for j = 1:allEvents{i}.numEvents
        if abs(sum(allEvents{i}.features(:,j))) > 0
            % Store new examples in buffer
            newTrainFeatures = [newTrainFeatures, allEvents{i}.features(:,j)];
            newTrainLabels = [newTrainLabels, allEvents{i}.eventTypes(j)];
            totalTrainInstances = totalTrainInstances + 1;
        end
    end
   
    % Compute associated objects of the events
    if pc.withOP
        newObjectsList = compute_object_list(allEvents, pc.numObjectClasses, i);
    end
    
    if iteration ~= 1
        if ~isempty(newTrainFeatures)
            % Get predictions from the current appearance model
            [pred, h] = softmaxPredict(softmaxModel, newTrainFeatures);
            
            % Make UGM data structures for the new video sequence
            newNumANode = size(newTrainFeatures, 2);
            newnStates = pc.numClasses * ones(1, newNumANode);
            
            if pc.withOP
                newNumONode = size(newObjectsList, 2);        
                newnStates = [newnStates, (pc.numObjectClasses-1) * ones(1, newNumONode)];
            end
            
            newMaxState = max(newnStates); 
            newNNodes = length(newnStates);

            % Make Adjacency Matrix and EdgeStruct for new data
            newAdj = zeros(newNNodes);
            [newAAdj, newTemporalDist, newSpatialDist] = ...
                make_adjacency(allEvents, i, pc.aa_thr_t, pc.aa_thr_t);
            
            newAdj(1:newNumANode, 1:newNumANode) = newAAdj;

            newANodes = 1:newNumANode;
            nodeNo = newNumANode;

            if pc.withOP
                % Link between activity and object
                for k = 1:newNumONode
                    nodeNo = nodeNo + 1;
                    newAdj(k, nodeNo) = 1;
                end
                newONodes = newNumANode+1:newNumANode+newNumONode;
            end
            newAdj = newAdj + newAdj';
            newEdgeStruct = UGM_makeEdgeStruct(newAdj,newnStates);
            
            % Make (non-negative) potential of each node taking each state
            newNodePot = zeros(newNNodes,newMaxState); 
            for n = 1:newNumANode
                for s = 1:newnStates(n)
                    newNodePot(n,s) = h(s,n);
                end
            end
           
            % Make (non-negative) potential of object nodes taking each state
            if pc.withOP
                for k = 1:length(newONodes);
                    n = newONodes(k); 
                    s = newnStates(n);
                    newNodePot(n,1:s) = newObjectsList(2:s+1,k) + pc.ONodeTweak;            
                end
            end

            % Make (non-negative) potential of each edge taking each state combination
            newEdgePot = zeros(newMaxState,newMaxState,newEdgeStruct.nEdges);
            nNewEdges = newEdgeStruct.nEdges;
            
            for e = 1:nNewEdges
                n1 = newEdgeStruct.edgeEnds(e,1);
                n2 = newEdgeStruct.edgeEnds(e,2);
                % A-A edges
                if ismember(n1,newANodes) && ismember(n2,newANodes)
                    td = newTemporalDist(n1,n2);
                    sd = newSpatialDist(n1,n2);
                    newEdgePot(:,:,e) = pc.coFreqAA_m * coFreqAA + td + sd + pc.coFreqAA_a;
                end
                % A-O or O-A edges
                if pc.withOP
                    if (ismember(n1,newANodes)&&ismember(n2,newONodes))||(ismember(n1,newONodes)&&ismember(n2,newANodes))
                        if newnStates(n1) > newnStates(n2)             
                            s1 = newnStates(n1);
                            s2 = newnStates(n2);
                        else
                            s2 = newnStates(n1);
                            s1 = newnStates(n2);
                        end
                        newEdgePot(1:s1,1:s2,e) = pc.coFreqAO_m * coFreqAO(1:s1,2:s2+1) + pc.coFreqAO_a;
                    end
                end
            end
            
            % Run inference on the CRF for new video sequence
            [nodeProbs,edgeProbs,~] = UGM_Infer_LBP(newNodePot,newEdgePot,newEdgeStruct);
            [~,weakLabels] = max(nodeProbs,[],2); 
            weakLabels = weakLabels(1:newNumANode)';
            
            % Active Learning Module
            [~, strongIndex] = active_learning_UGM(nodeProbs,...
                edgeProbs, newNumANode, newEdgeStruct, pc.K, pc.delta,...
                pc.strongTeacher, pc.weakTeacher);
            
            % Run conditional inference
            if pc.weakTeacher == 1;
                clamped = zeros(1,newNNodes);
                clamped(strongIndex) = newTrainLabels(strongIndex);
                [condNodePot,condEdgePot,~] = ...
                    UGM_Infer_Conditional(newNodePot, newEdgePot, ...
                    newEdgeStruct, clamped, @UGM_Infer_LBP);

                nodePot1 = condNodePot(1:newNumANode,:);
                nodePot1(nodePot1==1) = -1;
                weakIndex = find(max(nodePot1,[],2)>pc.delta);
            else
                weakIndex = [];
            end
            
            % Store the newly labeled data in the buffer
            bufferTrainFeatures = [bufferTrainFeatures, newTrainFeatures(:,strongIndex)];
            bufferTrainLabels = [bufferTrainLabels, newTrainLabels(strongIndex)];
            bufferTrainFeatures = [bufferTrainFeatures, newTrainFeatures(:,weakIndex)];
            bufferTrainLabels = [bufferTrainLabels, weakLabels(weakIndex)];
            
            totalManLabledIns = totalManLabledIns + length(strongIndex);
        end
    else       
        % Store new examples in buffer for initial training
        bufferTrainFeatures = [bufferTrainFeatures, newTrainFeatures];
        bufferTrainLabels = [bufferTrainLabels, newTrainLabels];        
        totalManLabledIns = totalManLabledIns + length(newTrainLabels);
    end
    
    % Gather context features
    
    % Co-occurance frequencies among the activities
    curTrainSeq = [curTrainSeq, i];
    coFreqAA = co_frequency(allEvents, curTrainSeq, pc.numClasses, pc.aa_thr_t, pc.aa_thr_s);
    
    if pc.withOP
        % Co-occurance frequencies of the activity and objects
        coFreqAO = compute_co_freq_AO(coFreqAO, newTrainLabels, newObjectsList, pc.numClasses);
        % Activity people context
        types = allEvents{i}.eventTypes;
        for j = 1:allEvents{i}.numEvents
            for k = 1:length(allEvents{i}.mapObjectsTypes{j})
               if allEvents{i}.mapObjectsTypes{j}(k)==1
                   fl = allEvents{i}.mapObjectsFirstLocation{j}(k,1:2);
                   ll = allEvents{i}.mapObjectsLastLocation{j}(k,1:2);
                   dist = norm(fl-ll,2);
                   bin = bin_P_dist(dist);
                   contextAP(types(j),:) = contextAP(types(j),:) + bin';
               end
           end    
        end
    end
    
    % If buffer is full update the baseline classifier
    if totalTrainInstances>=iteration*trainInstancesPerIter || i==pc.trainSeq(end) 
        iteration = iteration+1;
        
        % Update the appearance model
        options.maxIter = 400;
        softmaxModel = softmaxTrainNew(pc.inputSize, pc.numClasses, pc.lambda, ...
               bufferTrainFeatures, bufferTrainLabels, options);
        
        % Get the labels from the appearance model   
        [pred, h] = softmaxPredict(softmaxModel, testFeatures);
        
        [~,res_a] = max(h,[],1);
        acc3 = [acc3, mean(testLabels' == res_a(:))];
        
        % Make (non-negative) potential of each node taking each state
        testNodePot = zeros(testnNodes,testMaxState); 
        for n = 1:testNumANode
            s = testnStates(n);
            testNodePot(n,1:s) = h(1:s,n);
        end
        
        if pc.withOP
            % Make (non-negative) potential of object nodes taking each state
            for k = 1:length(testONodes);
                n = testONodes(k); 
                s = testnStates(n);
                testNodePot(n,1:s) = testObjectsList(2:s+1,k) + pc.ONodeTweak;            
            end

            % Make (non-negative) potential of people nodes taking each state
            assert(length(PNodes)==size(testPDist,2));
            for k = 1:length(PNodes);
                n = PNodes(k); 
                for s = 1:testnStates(n)
                    testNodePot(n,s) = testPDist(s,k) + pc.PNodeTweak;
                end
            end
        end
                 
        % Make (non-negative) potential of each edge taking each state combination
        testEdgePot = zeros(testMaxState,testMaxState,testEdgeStruct.nEdges);
        nEdges = testEdgeStruct.nEdges;
        for e = 1:nEdges
            n1 = testEdgeStruct.edgeEnds(e,1);
            n2 = testEdgeStruct.edgeEnds(e,2);
            
            % A-A edges
            if ismember(n1,testANodes) && ismember(n2,testANodes)
                td = testTemporalDist(n1,n2);
                sd = testSpatialDist(n1,n2);
                testEdgePot(:,:,e) = pc.coFreqAA_m * coFreqAA + td + sd + pc.coFreqAA_a;
            end
        
            if pc.withOP
                % A-O or O-A edges
                if (ismember(n1,testANodes)&&ismember(n2,testONodes))||(ismember(n1,testONodes)&&ismember(n2,testANodes))
                    if testnStates(n1) > testnStates(n2)             
                        s1 = testnStates(n1);
                        s2 = testnStates(n2);
                    else
                        s2 = testnStates(n1);
                        s1 = testnStates(n2);
                    end
                    testEdgePot(1:s1,1:s2,e) = pc.coFreqAO_m * coFreqAO(1:s1,2:s2+1) + pc.coFreqAO_a;
                end

                % A-P or P-A edges
                if (ismember(n1,testANodes)&&ismember(n2,PNodes))||(ismember(n1,PNodes)&&ismember(n2,testANodes))
                    if testnStates(n1) > testnStates(n2)             
                        s1 = testnStates(n1);
                        s2 = testnStates(n2);
                    else
                        s2 = testnStates(n1);
                        s1 = testnStates(n2);
                    end
                    testEdgePot(1:s1,1:s2,e) = pc.contextAP_m * contextAP + pc.contextAP_a;
                end
            end
        end
        
        [nodeProbs,edgeProbs,logZ] = UGM_Infer_LBP(testNodePot,testEdgePot,testEdgeStruct);
        [~,res_infer] = max(nodeProbs,[],2);
        acc2 = [acc2, mean(testLabels' == res_infer(1:testNumANode))];
        
        h = nodeProbs(1:testNumANode,:);
        
        fprintf('Batch - %d accuracies: %0.3f---%0.3f\n', iteration-1, ...
            acc3(end)*100, acc2(end)*100);
         
        res_noContext(iteration-1,:) = res_a';
        resContext_infer(iteration-1,:) = res_infer(1:testNumANode);
        oldSize = size(bufferTrainFeatures,2);
    end
end
fprintf('Number of manually labeled instances: %d/%d\n', totalManLabledIns,totalTrainInstances);
fprintf('Number of test instances: %d\n', length(testLabels));