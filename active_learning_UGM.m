function [weakIndex, strongIndex] = active_learning_UGM(nodeProbs, ...
    edgeProbs, numANodes, edgeStruct, K, delta, strongTeacher, weakTeacher)

h = nodeProbs(1:numANodes, :)';

% Weak teacher
weakIndex = [];
if weakTeacher==1
    weakIndex = find(max(h)>delta);
end

% Strong teacher
strongIndex = [];
if strongTeacher==1
    strongIndex = strong_teacher_em(h, edgeProbs, numANodes, edgeStruct, K);
end

% All manual labeling
if weakTeacher == 0 && strongTeacher == 0
    strongIndex = 1:numANodes;
end

% CVPR Method
if weakTeacher == 1 && strongTeacher == 3
    h = h';
    strongIndex = strong_teacher_cvpr(h, K);
end

% Only entropy
if weakTeacher == 4 && strongTeacher == 4
    strongIndex = strong_teacher_e(h, K);
end

strongIndex = setdiff(strongIndex,weakIndex);